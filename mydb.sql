--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: books; Type: TABLE; Schema: public; Owner: shaun; Tablespace: 
--

CREATE TABLE books (
    id integer NOT NULL,
    title character varying(255),
    size character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    approved boolean DEFAULT false,
    total character varying(255),
    cat character varying(255),
    user_id integer
);


ALTER TABLE public.books OWNER TO shaun;

--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: shaun
--

CREATE SEQUENCE books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.books_id_seq OWNER TO shaun;

--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shaun
--

ALTER SEQUENCE books_id_seq OWNED BY books.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: shaun; Tablespace: 
--

CREATE TABLE ckeditor_assets (
    id integer NOT NULL,
    data_file_name character varying(255) NOT NULL,
    data_content_type character varying(255),
    data_file_size integer,
    assetable_id integer,
    assetable_type character varying(30),
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.ckeditor_assets OWNER TO shaun;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: shaun
--

CREATE SEQUENCE ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ckeditor_assets_id_seq OWNER TO shaun;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shaun
--

ALTER SEQUENCE ckeditor_assets_id_seq OWNED BY ckeditor_assets.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: shaun; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO shaun;

--
-- Name: snippets; Type: TABLE; Schema: public; Owner: shaun; Tablespace: 
--

CREATE TABLE snippets (
    id integer NOT NULL,
    content character varying(255),
    book_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    approved boolean DEFAULT false,
    user_id integer
);


ALTER TABLE public.snippets OWNER TO shaun;

--
-- Name: snippets_id_seq; Type: SEQUENCE; Schema: public; Owner: shaun
--

CREATE SEQUENCE snippets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.snippets_id_seq OWNER TO shaun;

--
-- Name: snippets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shaun
--

ALTER SEQUENCE snippets_id_seq OWNED BY snippets.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: shaun; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    rank character varying(255) DEFAULT '0'::character varying,
    first_name character varying(255),
    last_name character varying(255),
    profile_name character varying(255),
    admin boolean DEFAULT false,
    sash_id integer,
    level integer DEFAULT 0,
    provider character varying(255),
    uid character varying(255),
    name character varying(255)
);


ALTER TABLE public.users OWNER TO shaun;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: shaun
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO shaun;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shaun
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: shaun
--

ALTER TABLE ONLY books ALTER COLUMN id SET DEFAULT nextval('books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: shaun
--

ALTER TABLE ONLY ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('ckeditor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: shaun
--

ALTER TABLE ONLY snippets ALTER COLUMN id SET DEFAULT nextval('snippets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: shaun
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: shaun
--

COPY books (id, title, size, created_at, updated_at, approved, total, cat, user_id) FROM stdin;
1	Short	0	2013-10-29 12:19:53.173542	2013-10-29 12:20:00.168518	t	\N	Sci-Fi	1
\.


--
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shaun
--

SELECT pg_catalog.setval('books_id_seq', 1, true);


--
-- Data for Name: ckeditor_assets; Type: TABLE DATA; Schema: public; Owner: shaun
--

COPY ckeditor_assets (id, data_file_name, data_content_type, data_file_size, assetable_id, assetable_type, type, width, height, created_at, updated_at) FROM stdin;
\.


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shaun
--

SELECT pg_catalog.setval('ckeditor_assets_id_seq', 1, false);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: shaun
--

COPY schema_migrations (version) FROM stdin;
20131023181919
20131023175240
20131021095707
20131019205738
20131022121423
20131010100000
20131019210813
20131019172052
20131016075818
20131023104117
20131021094140
20131016083215
20131016080836
20131023103020
20131019191853
20131023140314
20131023140618
20131023111258
20131029100501
20131029155849
\.


--
-- Data for Name: snippets; Type: TABLE DATA; Schema: public; Owner: shaun
--

COPY snippets (id, content, book_id, created_at, updated_at, approved, user_id) FROM stdin;
1	Abby!	1	2013-10-29 12:20:10.479029	2013-10-29 12:20:10.490557	f	1
\.


--
-- Name: snippets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shaun
--

SELECT pg_catalog.setval('snippets_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: shaun
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, rank, first_name, last_name, profile_name, admin, sash_id, level, provider, uid, name) FROM stdin;
2	user@user.com	$2a$10$AgD35ekp5znUH6BjyPX5cexxdZdSVaxEz5iLcpu.7yNYO90zA55ri	\N	\N	\N	0	\N	\N	\N	\N	2013-10-29 10:32:37.685565	2013-10-29 10:32:37.685565	clerk	user	man	profilename	f	\N	0	\N	\N	\N
1	admin@admin.com	$2a$10$4K/XxWdVFjZGTimbglEAKuFaqwrqkDLlN.L/3tQb5.VcjrXDFDjBS	\N	\N	2013-10-29 12:19:35.031362	3	2013-10-29 16:45:13.331974	2013-10-29 12:19:35.050227	127.0.0.1	127.0.0.1	2013-10-29 10:32:37.49751	2013-10-29 16:45:13.334792	clerk	admin	minstrator	profilename	t	\N	0	\N	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shaun
--

SELECT pg_catalog.setval('users_id_seq', 2, true);


--
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: shaun; Tablespace: 
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: shaun; Tablespace: 
--

ALTER TABLE ONLY ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: snippets_pkey; Type: CONSTRAINT; Schema: public; Owner: shaun; Tablespace: 
--

ALTER TABLE ONLY snippets
    ADD CONSTRAINT snippets_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: shaun; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: idx_ckeditor_assetable; Type: INDEX; Schema: public; Owner: shaun; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable ON ckeditor_assets USING btree (assetable_type, assetable_id);


--
-- Name: idx_ckeditor_assetable_type; Type: INDEX; Schema: public; Owner: shaun; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable_type ON ckeditor_assets USING btree (assetable_type, type, assetable_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: shaun; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: shaun; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: shaun; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


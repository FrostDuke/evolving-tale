# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :admin, class: User do
    first_name "admin1"
    last_name "minstrator"
    password "admin1234"
    profile_name "profilename"
    email "admin1@admin.com"
    password_confirmation "admin1234"
    admin true
    end
    
    factory :user, class: User do
    sequence(:first_name) { |n| "Person #{n}" }
    sequence(:last_name) { |n| "last#{n}"}
    sequence(:profile_name) { |n| "profile#{n}" }
    password "user1234"
    sequence(:email) { |n| "last_#{n}@example.com"}
    password_confirmation "user1234"
    admin false
    end

    factory :book, class: Book do
    sequence(:title) { |n| "Book#{n}" }
    approved true
    size  0
    cat "Sci-Fi"
    user
    end

    factory :snippet, class: Snippet do
    sequence(:content) { |n| "Snippet#{n}" }
    approved true
    user
    book
    end
    
  end
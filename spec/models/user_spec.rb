require 'spec_helper'
require "cancan/matchers"


describe User do

  before do
    @user = User.new(first_name: "Shaun", last_name: "Jackson", profile_name: "Shauny9k", 
      email: "shaun999@gmail.com", password: "foobar", password_confirmation: "foobar")
  end

  it { should respond_to(:snippets)}

  context "sign up validation" do
    let(:user) { User.create(nil) }
     
      its(:first_name) { should raise_error }
      its(:last_name) { should raise_error }
      its(:profile_name) { should raise_error }
      its(:password) { should raise_error }
   end
  

  describe "abilities" do
    subject(:ability){ Ability.new(user) }
    let(:user){ nil }

    context "when is a clerk" do
        let(:user) {User.create(:rank => "Clerk")}
          it { should_not be_able_to(:create, Book.new) }
          it { should be_able_to(:create, Snippet.new)}
    end
     
    context "when is a author" do
        let(:user) {User.create(:rank => "Author")}
          it {should_not be_able_to(:create, Book.new)}
          it {should be_able_to(:create, Snippet.new)}
      end 

      context "when is a Co Editor" do
        let(:user) {User.create(:rank => "Co-Editor")}
          it {should_not be_able_to(:create, Book.new)}
          it {should be_able_to(:create, Snippet.new)}
          it {should be_able_to(:approve, Snippet.new)}
      end 

      context "when is a Chief Editor" do
        let(:user) {User.create(:rank => "Chief Editor")}
          it {should be_able_to(:create, Book.new)}
          it {should be_able_to(:create, Snippet.new)}
          it {should be_able_to(:approve, Snippet.new)}
          it {should be_able_to(:approve, Book.new)}
      end
    end

  describe "snippet assoociations" do
   before {@user.save}
    let!(:older_snippets) do
      FactoryGirl.create(:snippet, user: @user, created_at: 1.day.ago)
    end
    let!(:newer_snippets) do
      FactoryGirl.create(:snippet, user: @user, created_at: 1.hour.ago)
    end

       it "should have the right order for snippets" do
       @user.snippets.should == [older_snippets, newer_snippets]
       end
    end

    describe "ranking" do   
    let!(:user) {FactoryGirl.create(:user)}
    let!(:book)  {FactoryGirl.create(:book, user_id: user)}
    let!(:snippet) do
      1.times do
      FactoryGirl.create(:snippet, user_id: user.id, book_id: book.id)
      end
    end

      it "should rank up" do
       @user.points.should == "10"
      end

   end
end


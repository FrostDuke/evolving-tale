require 'spec_helper'

describe Snippet do
 it "should not exceed words per snippet"
 it "should not contain links to other websites"
 it "should be linked to a user_id"
 it "should be linked to a book_id"
end

class AddDefaultApprovedToSnippet < ActiveRecord::Migration
  def change
    remove_column :snippets, :approved
  end
end

class AddTotalWordsToBooks < ActiveRecord::Migration
  def change
    add_column :books, :total, :string
  end
end

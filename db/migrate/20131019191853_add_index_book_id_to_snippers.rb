class AddIndexBookIdToSnippers < ActiveRecord::Migration
  def change
    add_column :books, :book_id, :integer
    add_index :books, :book_id
  end
end

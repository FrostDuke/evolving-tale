class CreateSnippets < ActiveRecord::Migration
  def change
    create_table :snippets do |t|
      t.string :content
      t.integer :book_id
      t.boolean :approved

      t.timestamps
    end
  end
end

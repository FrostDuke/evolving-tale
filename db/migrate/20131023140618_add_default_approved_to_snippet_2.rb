class AddDefaultApprovedToSnippet2 < ActiveRecord::Migration
  def change
    add_column :snippets, :approved, :boolean, :default => false
  end
end

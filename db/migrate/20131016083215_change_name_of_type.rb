class ChangeNameOfType < ActiveRecord::Migration
  def change
    rename_column :books, :type, :size
  end
end

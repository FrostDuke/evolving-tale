require 'rubygems'
require 'factory_girl_rails'

# Create the admin users
FactoryGirl.define do
  factory :admin1, class: User do
    first_name "admin"
    last_name "minstrator"
    password "admin1234"
    profile_name "profilename"
    email "admin@admin.com"
    password_confirmation "admin1234"
    admin true
    end
    
    factory :user1, class: User do
    first_name "user"
    last_name "man"
    password "user1234"
    profile_name "profilename"
    email "user@user.com"
    password_confirmation "user1234"
    admin false
    end
    
  end
FactoryGirl.create(:admin1)
FactoryGirl.create(:user1)

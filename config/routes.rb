EvolvingFinal::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations" } 


  root to: 'pages#home'
  resources :admins
  resources :users
 
  resources :books do
     resources :snippets, :only => [:create, :edit, :update, :destroy]
  end
  

  #get 'sign_in', :to => 'users/sessions#new', :as => :new_session
  post "books/:id/activate" => "books#approve", :as => "active_book"
  post "snippets/:id/activate" => "snippets#approve", :as => "active_snippet"

  put "books/:book_id/like", to: "books#upvote", :as => "book_like"
  put "books/:book_id/dislike", to: "books#downvote", :as => "book_dislike"
 

end

# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
EvolvingFinal::Application.initialize!


Time::DATE_FORMATS[:create_date] = "created in %B"


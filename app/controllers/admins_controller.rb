class AdminsController < ApplicationController
   before_filter :authenticate_user!

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
    @books = Book.where(approved: false)
    @snippets = Snippet.where(approved: false)
  end

  def show
    @snippet = Snippet.where(approved: false)
    @user = User.find(params[:id])
    @book = Book.where(approved: false)
  end
   
  def update
    @user = User.find(params[:rank])
     if @user.save
       @user.update[:rank]
     end
  end
  
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end
end


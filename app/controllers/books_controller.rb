class BooksController < ApplicationController
  load_and_authorize_resource
  before_filter :authenticate_user!
  before_action :set_book, only: [:show, :edit, :update, :destroy ]
  
  #before_action :content_check
  # GET /books
  # GET /books.json
  def index
     @books = Book.where(approved: true)
     @books = @books.where(:cat => params[:cat]) if params[:cat]   
     @snippets = Snippet.where(approved: true)
  end
  
  def approve
     @book = Book.find(params[:id])
    if @book.update_attribute(:approved, true)
      redirect_to admins_path
    else
    render root_path
   end 
end

  # GET /books/1
  # GET /books/1.json
  def show
    if Book.where(approved: true)
    @book = Book.find(params[:id])
    @snippets = @book.snippets.where(approved: true).paginate(:page => params[:page], :per_page => 10)
    @snippet = @book.snippets.new
    @count = @snippets.map {|e| e.info_panel_words(@book).to_i }
    @count = @count.inject(:+)
    else
      redirect_to root_path
      flash[:alert] = "Submitted for approval."
    end
  end

  # GET /books/new
  def new
    @book = Book.new
    
  end

  # GET /books/1/edit
  def edit
  end
  
  # POST /books
  # POST /books.json
  def create
    @book = Book.new(book_params)
    @book.user = current_user
    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: 'Book was successfully submitted for approval.' }
        format.json { render action: 'show', status: :created, location: @book }
      else
        format.html { render action: 'new' }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url }
      format.json { head :no_content }
    end
  end

def upvote
 raise book_params.inspect
  @book = Book.find(params[:id])
  @book.liked_by current_user(book_params)
  redirect_to books_path
end

def downvote
  @book = Book.find(params[:book_id])
  @book.downvote_from current_user
  redirect_to books_path
end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:title, :size, :cat, :user_id, :votable, :voter, :vote_scope
)
    end
end

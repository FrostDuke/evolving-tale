class SnippetsController < ApplicationController
load_and_authorize_resource
  before_filter :authenticate_user!
  before_filter :find_book, :except => [:approve]
  
   def create
    binding.pry
   @snippet = @book.snippets.create(snip_params)
   @snippet.user = current_user
     if @snippet.save
      redirect_to @book
      flash[:success] = "Snippet submitted and awaiting approval."
    else
      flash[:base] = "Someone else has submitted a snippet, please try again later"
      redirect_to @book
   end
 end

  def destroy
    @snippet.destroy
    respond_to do |format|
      format.html { redirect_to books_url }
      format.json { head :no_content }
    end
  end     
  
  def approve

      @snippet = Snippet.find(params[:id])
    if @snippet.update_attribute(:approved, true)
      redirect_to admins_path
    else
    render root_path
   end 
  end

  def edit
    @snippet = @book.snippets.find(params[:id])
  end    
  
  def update
    @snippet = @book.snippets.find(params[:id])
    respond_to do |format|
      if @snippet.update_attributes(params[:snippet])
        format.html { redirect_to @book, notice: 'Comment was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  private

  def find_book
    #raise params.inspect
    @book = Book.find(params[:book_id])
  end

  def snip_params
      params.require(:snippet).permit(:content, :approved, :user_id)
    end
end
  

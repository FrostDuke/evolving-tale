module ApplicationHelper

    def flash_class(type)
    case type
    when :alert
      "alert-error"
    when :notice
      "alert-success"
    else
      ""
    end
  end
 


 def newest_snippet
  Snippet.newest
 end

 def ranked
  User.limit(5).ranked
 end

 def newest
  Book.newest
 end

end

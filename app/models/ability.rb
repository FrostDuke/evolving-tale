class Ability
  include CanCan::Ability

   def initialize(user)
    user ||= User.new # guest user (not logged in)
     if user.admin?
      can :manage, :all
      can :approve, :all
     else 
      #clerk abilities
      cannot :create, Book if user.rank == "Clerk"
      can :read, Book if user.rank == "Clerk"
      can :create, Snippet if user.rank == "Clerk"
      
      #author abilities
       cannot :create, Book if user.rank == "Author"
      can :read, Book if user.rank == "Author"
      can :create, Snippet if user.rank == "Author"
      can :read, Book if user.rank == "Author"

      #co-editor
      cannot :create, Book if user.rank == "Co-Editor"
      can :read, Book if user.rank == "Co-Editor"
      can :create, Snippet if user.rank == "Co-Editor"
      can :approve, Snippet if user.rank == "Co-Editor"
     
      #Chief Editor abilities
      can [:read, :create, :approve], [Book, Snippet] if user.rank == "Chief Editor"
     
    end
  end
end
class User < ActiveRecord::Base 
  require 'carrierwave/orm/activerecord'
  acts_as_voter
  before_create :create_rank
  has_merit
  
  validates :first_name, :last_name, :profile_name, presence: true
 # validates :email, presence: true,
             #  :format => { :with => /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i, :on => :create },
              # :uniqueness => true,
               #:multiline => true
  validates :password, :presence => true,
                       :confirmation => true,
                       :length => {:within => 6..40},
                       :on => :create

  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :registerable,
         :omniauthable, :omniauth_providers => [:facebook] 

#  attr_accessible :email, :password, :password_confirmation, :first_name, :last_name,
 #                 :profile_name, :avatar, :avatar_cache, :remove_avatar, :rank, :provider, 
  #                :uid, :name, :sash_id, :badge_id, :votable, :voter, :vote_scope
         

  mount_uploader :avatar, AvatarUploader
  #validates_presence_of   :avatar
  #validates_integrity_of  :avatar
  #validates_processing_of :avatar
  
  has_many :books
  has_many :snippets

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end


 def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
  user = User.where(:provider => auth.provider, :uid => auth.uid).first
  unless user
    user = User.create(name:auth.extra.raw_info.name,
                         first_name:auth.extra.raw_info.first_name,
                         last_name:auth.extra.raw_info.last_name,
                         profile_name:auth.extra.raw_info.name,
                         avatar: auth.info.image,
                         provider:auth.provider,
                         uid:auth.uid,
                         email:auth.info.email,
                         password:Devise.friendly_token[0,20]
                         )
  end
  user
end

 def self.ranked
  User.all.sort_by {|e| e.books.where(:approved => true).count + e.snippets.where(:approved => true).count }
 end
 

  def self.approved
    Book.approved + Snippet.approved
  end
  
   def user_rank
    #binding.pry
    sn = self.points
    if sn >= 15
     self.update_attributes(:rank => "Author")
    elsif sn > 50 && sn.count >= 60
     self.update_attributes(:rank => "Co-Editor")
    elsif sn > 95 && sn.count >= 100
     self.update_attributes(:rank => "Chief Editor")
    else
      self.update_attributes(:rank => "Clerk")
     end
    end


  
         
     def create_rank
       self.rank = "Clerk"
     end
     
     def full_name
       name = "#{first_name.capitalize} #{last_name.capitalize}"
     end


end

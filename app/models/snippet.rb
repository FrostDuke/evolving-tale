class Snippet < ActiveRecord::Base
  include ActionView::Helpers::SanitizeHelper 
  after_update :update_user_rank
  belongs_to :book
  belongs_to :user
  attr_accessible :content, :book_id , :approved, :user_id
  attr_reader :info_panel_words
  validate :stop_create, :on => :create
  validates :book_id, presence: true
  validate :size_limit

  def stop_create
    errors.add(:base, "could not add due to last snippet not approved") if Snippet.last.try(:approved) == false
   end
   
  def update_user_rank
    user.user_rank
  end 
   
self.default_scope order('created_at ASC')
 

 scope :approved, -> { where(approved: true) }



  BOOK_SIZE = { 
    0 => {'per' => 500, 'total' => 15000},
    1 => {'per' => 700 , 'total' => 30000},
    2 => {'per' => 1000, 'total' => 50000}
  }
  
  def info_panel_words(snippet)
      stripped_content = strip_tags(content)
      word_count = stripped_content.scan(/\w+/).size.to_i
      current_size = (self.book.title_count || 0) + word_count
  end

   def size_limit 
    stripped_content = strip_tags(content)
    book_limit = self.book.size.to_i
    word_count = stripped_content.scan(/\w+/).size.to_i
    current_snippets_size = (self.book.get_word_count || 0) + word_count 
    errors.add(:base, "Content size is too big") unless word_count < BOOK_SIZE[book_limit]['per'] && current_snippets_size < BOOK_SIZE[book_limit]['total']    
  end

 def self.newest
  Snippet.order('created_at DESC').limit(5).all
  end

end


class Book < ActiveRecord::Base
  include ActionView::Helpers::SanitizeHelper 
  acts_as_votable
  after_update :update_user_rank
  has_many :snippets 
  belongs_to :user
  #attr_accessible :title, :book_id, :size, :cat, :approved, 
   #               :user_id, :votable, :voter, :vote_scope

  validates :title, presence: true,
                    uniqueness: true
  validates :size, presence: true
  validates :cat, presence: true
  scope :approved, -> { where(approved: true) }
  
  def self.newest
  Book.order('created_at DESC').limit(5).all
  end
  
  def title_count
    stripped_tags = strip_tags(title)
    word_count = stripped_tags.scan(/\w+/).size.to_i
  end

   def update_user_rank
    user.user_rank
   end 

  def get_word_count
    @word_count = []
    self.snippets.each do |c|
      stripped_tags = strip_tags(c.content)
      @word_count << stripped_tags.scan(/\w+/).size.to_i
    end
    @word_count = @word_count.inject(:+)
  end
 
  def short?
   size == 0
  end
  
  def medium?
    size == 1
  end
  
  def long?
    size == 2
  end
end
